# Patches already applied upstream or committed in a side branch
# Or nearly-finished patches that they'll very probably merge.
u0-gap-util.patch
u0-gap-trac-26889.patch                    #26889
u0-version-gap-4.10.patch                  #22626
u0-gap-trac-26856.patch                    #26856
u0-version-networkx-2.2.patch              #26326
u0-version-numpy-1.14.3.patch              #25260

# Patches that have open upstream tickets
u1-fix-atrocious-symlink-logic.patch       #22444
u1-looser-doctest-basename.patch           #22445
u1-scripts-dir.patch                       #22731
u1-ipywidgets-repr.patch                   #https://github.com/jupyter-widgets/ipywidgets/pull/1031
u1-workaround-sympow-malloc-perturb.patch  #https://salsa.debian.org/science-team/sympow/merge_requests/1, reported to upstream by private email
u1-fix-trivial-test-cases.patch            #25094, #25096, #25102, the interpreter bits do not seem necessary anymore
u1-fix-mkdirs-sage-kernel-spec.patch       #26176

# Patches that should be upstreamed (may need some work first)
u2-fix-sympow-cachedir.patch
u2-allow-override-sage-local.patch
u2-version-r-3.5.1-incomplete.patch        #26185

# Patch Sage to work with dependency Debian packages
# These won't change even if Debian and Sage use the same version
# Not suitable for upstreaming
d0-arb.patch
d0-gsl-cblas.patch
d0-libgap-path.patch
d0-maxima.patch
d0-nauty.patch
d0-rubiks.patch
d0-singular.patch
d0-disable-jsmol.patch
d0-paths.patch
d0-threejs-offline-paths.patch

# Patch Sage to behave as a Debian package
# Not suitable for upstreaming
d1-install-paths.patch # many things in here could be split into an upstreamable patch
d1-sage-cli.patch
d1-sage-env.patch
d1-fakeroot.patch
d1-multiarch-python-paths.patch
d1-doc-docs.patch
d1-disable-post-install-tests.patch

# Potentially controversial fixes
# We had to do these to make things work, at some time in the past
# It may be possible to drop these now or in the future; test that first.
df-revert-minor-feature-dependent-on-python-patch.patch
df-python_security.patch
df-sage-gap-reset-doctest.patch
df-silence_ecl_opt_signals.patch
df-subprocess-sphinx.patch

# Temporary patches for whatever reason
# They will eventually be gotten rid of
dt-version-glpk-4.60-extra-hacky-fixes.patch
dt-version-glpk-4.65-ignore-warnings.patch
dt-more-fix-test-cases.patch
dt-work-around-doc-common-conf.patch
dt-work-around-singular-mips64el-segfault.patch
dt-version-ipywidgets-6-revert-23177.patch
